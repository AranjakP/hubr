<?php

namespace app\models;

use Yii;
use yii\base\Model;
use Exception;
use yii\httpclient\Client;
use yii\httpclient\Request;
use yii\httpclient\Responce;
use keltstr\simplehtmldom\SimpleHTMLDom;


class Hubr extends Model
{

    public $articles;
    public $client;

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        parent::init();
        $this->client = new Client([
            'transport' => 'yii\httpclient\CurlTransport'
        ]);
    }

    public function process()
    {

        $articles = [];

        try {
            $request = $this->client->get('https://habr.com/ru/');
            $responce = $request->send();
            // $html = SimpleHTMLDom::file_get_html('https://habr.com/ru/');
            $html = SimpleHTMLDom::str_get_html($responce->content);

            $pages_count =  sizeof($html->find('#nav-pagess li'));
            
            for ($page = 1; $page <= min($pages_count, 4); $page++) {
                if ($page > 1) {
                    $request = $this->client->get('https://habr.com/ru/page'.$page.'/');
                    $responce = $request->send();
                    $html = SimpleHTMLDom::str_get_html($responce->content);
                }
                    
                $posts = $html->find('li.content-list__item_post article');

                foreach ($posts as $element) {
                    $href = $element->find('a.post__title_link')[0]->href;
                    $title = $element->find('a.post__title_link')[0]->text();
                    $views = $element->find('.post-stats__views-count ')[0]->text();
                    if (strpos($views, 'k') !== false) {
                        $count = (float)str_replace(',', '.', str_replace('k', '', $views)) * 1000;
                    } else {
                        $count = (float)str_replace(',', '.', $views) * 1;
                    }
                    $articles[] = array('title' => $title, 'href' => $href, 'views' => $count);
                }
            }
            usort($articles, function ($a, $b) {
                return $b['views'] <=> $a['views'];
            });
            $this->articles = $articles;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

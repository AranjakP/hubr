<?php

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

use app\models\Hubr;

class HubrController extends Controller
{
    public function actionIndex($message = 'hello Hubr')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }

    public function actionShow()
    {
        $hubr = new Hubr();
        $hubr->process();


        $articles = $hubr->articles;
        foreach ($articles as $article) {
            echo $article['title'] . ' ' . $article['views'] . "\n";
        }

        return ExitCode::OK;
    }
}

<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Hubr';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-hubr">
    <h1><?= Html::encode($this->title) ?></h1>
    <ul class="list-group">
        <?php
        foreach ($articles as $article) { ?>
            <li class="list-group-item"><a href="<?= $article['href'] ?>"><?= $article['title'] ?></a><span class="badge"><?= $article['views'] ?></span></li>
        <?php }
    ?>
    </ul>
</div>